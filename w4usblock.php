<?php
/**
 * @file
 * Weather for us block class.
 */

class WeatherforusBlock {


  /**
   * Renders a view file from views directory.
   */
  protected static function view($name) {
    ob_start();
    $root = W4S_ROOT;
    require "${root}/views/${name}.tpl.php";
    $ret = ob_get_contents();
    ob_end_clean();
    return $ret;
  }

  /**
   * Do listing of widget.
   */
  public static function dolist($delta, $save) {
    $block = array();
    $block[0]["info"] = t('Widget for weatherfor.us');
    return $block;
  }

  /**
   * Perform view action loading the appropriate view.
   */
  public static function doview($delta, $save) {
    $block = array();
    $block['subject'] = 'Weather';
    $view = variable_get('weatherforus_skin', 'explicit');
    $block['content'] = self::view($view);
    return $block;
  }

}
