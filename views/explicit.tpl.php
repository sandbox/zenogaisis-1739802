<?php
/**
 * @file
 * Explicit view
 */

?>
<iframe id="explicit" src="http://www.weatherfor.us/load.php<?php
$location = variable_get('weatherforus_location', '');
$out = '';
if (!empty($location)) :
  $out = '?' . http_build_query(array('location' => $location));
endif;
echo $out;
?>" allowtransparency="true" style="background: transparent; width: 720px; height: 250px; overflow: hidden;" frameborder="0" scrolling="no"></iframe>
