<?php
/**
 * @file
 * Minion view
 */

?>
<div style="margin: 0 auto; width: 250px; position: relative; ">
<?php
$params = array();
$location = variable_get('weatherforus_location', '');
if (!empty($location)) :
  $params['location'] = $location;
endif;
?>
  <script type="text/javascript" src="http://www.weatherfor.us/static/js/minion/minion.js"></script>
  <script type="text/javascript">
    w4uminion.run(<?php
echo json_encode($params);
?>);
  </script>
</div>
