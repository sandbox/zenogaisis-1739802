
-- SUMMARY --

By Installing this widget your users can see weather updates for 
their location on your website. You can get forecast, various units 
and really beautiful, eye pleasing weather status that instantly 
seeks your attention. Plugin loads from our optimised servers to 
server the best version according to browser loading the plugin 
and be as snappy as possible.

-- REQUIREMENTS --

None

-- INSTALLATION --
  
* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Administer > Site buildings > Modules > Core - Extended, enable Weatherfor.us
* Then go to Administer > Site buildings > Blocks
* From Disabled blocks select required Region for *Widget for weatherfor.us*

-- CUSTOMIZATION --

* Under Administer > Site configuration > Weather for us widget module
* In the panel you can custimize your widget type and location.

-- TROUBLESHOOTING --

None.


-- FAQ --

Q. Where can I ask for any information or trouble shooting?

A. info@weatherfor.us


-- CONTACT --

Current maintainers:
* Weather for us dev. - info@weatherfor.us
